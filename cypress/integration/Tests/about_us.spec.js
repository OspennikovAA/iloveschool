import {headerUrls} from './check_data/main_elements'
import {footerUrls} from './check_data/main_elements'
// Переменные для проверок слайдера
let sliderText = ''; // Проверка переключаемости
let readAll = ''; // Проверка смены карточек

describe('Тест страницы О нас', () => {

    it('Заходим на страницу', () => {cy.visit("/about")})
        // Тест Хэдера
    describe('Тест Хэдера', () => {

        it('Проверка ЛОГО и окошка регистрации/пользователя', () => {

            cy.get('.header__logo-block').should('have.attr', 'href', headerUrls[0])
            cy.get('.header__platform-block a').should('have.length', 5)
            cy.get('div[class*="profile-block"]').should('be.visible')

        })

        for (let i = 0; i < 5; i++) {

            it('Проверка ссылок', () => {
                cy.get('.header__platform-block a').eq(i).should('have.attr', 'href', headerUrls[i+1])
            })

        }

    })

    // Тест Футера

    describe('Тест футера', () => {

        it('Тест ссылок футера', () => {

            for(let i = 0; i < footerUrls.length; i++) {
                for(let j = 0; j < footerUrls[i].length; j++) {

                    cy.get('footer').children().children().eq(i).within(() => {

                        cy.get('a').eq(j).should('have.attr', 'href', footerUrls[i][j])

                    })

                }
            }

        })

    })

    // Тест тела страницы

    describe('Тест тела страницы', () => {

        it('Проверка структуры страницы', () => {

            cy.get('main .container-fluid').children().eq(0).should('have.class', 'welcome-section')
            cy.get('main .container-fluid').children().eq(1).should('have.class', 'platform-section')
            cy.get('main .container-fluid').children().eq(2).should('have.class', 'cover-section')
            cy.get('main .container-fluid').children().eq(3).should('have.class', 'platform-section')
            cy.get('main .container-fluid').children().eq(4).should('have.class', 'cover-section')
            cy.get('main .container-fluid').children().eq(5).should('have.class', 'platform-section')
            cy.get('main .container-fluid').children().eq(6).should('have.class', 'work-section')
            cy.get('main .container-fluid').children().eq(7).should('have.class', 'team-section')
            cy.get('main .container-fluid').children().eq(8).should('have.class', 'partners-section')


        })

        it('Проверка переключаемости слайдера', () => {

            for(let i =0; i < 4; i++) {
                cy.get('div[aria-hidden="false"] p').eq(0).then(($text) => {
                    expect($text.text()).to.not.equal(sliderText)
                    sliderText = $text.text()
                })
                cy.get('.team-section .btn-slider--left').click()
                cy.wait(500) 
            }

            for(let i =0; i < 4; i++) {
                cy.get('div[aria-hidden="false"] p').eq(0).then(($text) => {
                    expect($text).to.not.equal(sliderText)
                    sliderText = $text.text()
                })
                cy.get('.team-section .btn-slider--right').click()
                cy.wait(500) 
            }     

        })

        it('Проверка Читать полностью', () => {

            for(let i = 0; i < 4; i++) {

                cy.get('div[aria-hidden="false"] button').eq(0).click()
                cy.wait(300)

                cy.get('.modal-content__body')
                  .scrollIntoView()
                  .should('be.visible')

                cy.get('.modal-content__author').then(($autor) => {
                    expect($autor.text()).to.not.equal(readAll)
                    readAll = $autor.text()
                })
                
                cy.get('.modal-content__close-icon').click()
                cy.get('.modal-content').should('have.length', 0)

                cy.get('.team-section .btn-slider--left').click()
                cy.wait(500)

            }

        })

        it('Проверка ссылок на партнёров будет позже. На данный момент раздел забагован')

    })

})
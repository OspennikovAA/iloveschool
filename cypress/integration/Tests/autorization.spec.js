describe('Тест авторизации', () => {

    it('Заходим на страницу', () => {cy.visit("/")})

    describe('Тесты открытия и закрытия окна авторизации', () => {

        it('Открываем окно авторизации', () => {

            cy.get('.header__profile-block button').click()
            cy.get('.authorization-form__content').should('be.visible')
    
        })
    
        it('Закрываем кликом по крестику', () => {
    
            cy.get('.authorization-form__content i.icon--close').click()
            cy.get('.authorization-form__content').should('have.length', 0)
    
        })
    
        it('Открываем окно авторизации', () => {
    
            cy.get('.header__profile-block button').click()
            cy.get('.authorization-form__content').should('be.visible')
    
        })
    
        it('Закрываем кликом по экрану вне ока авторизации', () => {
    
            cy.get('.authorization-form').click('left')
            cy.get('.authorization-form__content').should('have.length', 0)
    
        })
    
        it('Открываем окно авторизации', () => {
    
            cy.get('.header__profile-block button').click()
            cy.get('.authorization-form__content').should('be.visible')
    
        })

    })

    describe('Тесты некорректного заполнения формы', () => {

        it('Пустые поля формы', () => {
            // Так находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear()
            // Так находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear()

            cy.get('.authorization-form__block input[name="email"]').clear()
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
              })
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

        it('Проверка чекбокса', () => {

            cy.get('.authorization-form__block label').click()
            cy.get('.authorization-form__block label').click()

        })

        it('Проверка корректного email с коротким паролем', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('test@sibdev.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear().type('1')
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Пароль должен состоять из минимум 6 символов').should('be.visible')
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

        it('Проверка email без @', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').type('test@sibdev.ru').clear().type('testsibdev.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear()
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.contains('Введите корректный адрес электронной почты.').should('be.visible')
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

        it('Проверка email без домена', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('test@sibdevru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear()
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.contains('Введите корректный адрес электронной почты.').should('be.visible')
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

        it('Проверка email без символов перед @', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('@testsibdev.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear()
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.contains('Введите корректный адрес электронной почты.').should('be.visible')
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

        it('Проверка email без символов между @ и точкой домена', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('testsibdev@.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear()
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.contains('Введите корректный адрес электронной почты.').should('be.visible')
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

        it('Проверка email с лишними @', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('test@sibdev@test@sibdev.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').type('123').clear()
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.contains('Данное поле должно быть заполнено.').should('be.visible')
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.contains('Введите корректный адрес электронной почты.').should('be.visible')
            })

            cy.get('.authorization-form__block button').should('have.attr', 'disabled')

        })

    })

    describe('Тесты корректного заполнения формы', () => {

        it('Проверка корректного заполнения незарегистрированным email', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('test@sibdev.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear().type('111111')
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })

            cy.get('.authorization-form__block button').should('not.have.attr', 'disabled')
            cy.get('.authorization-form__block button').click()
            cy.wait(300)

            cy.contains('Проверьте правильность введенных данных').should('be.visible')
            cy.contains('Проверьте правильность введенных данных').parent().within(() => {
                cy.get('button').click()
                cy.wait(500)
            })
            cy.contains('Проверьте правильность введенных данных').should('have.length', 0)

            cy.get('.authorization-form__block button').click()
            cy.wait(300)

            cy.contains('Проверьте правильность введенных данных').should('be.visible')
            cy.contains('Проверьте правильность введенных данных').parent().click('left')
            cy.wait(500)
            cy.contains('Проверьте правильность введенных данных').should('have.length', 0)

        })

        it('Проверка корректного заполнения зарегистрированным email и неправильным паролем', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('marazm01@yandex.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear().type('111111')
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })

            cy.get('.authorization-form__block button').should('not.have.attr', 'disabled')
            cy.get('.authorization-form__block button').click()
            cy.wait(300)

            cy.contains('Проверьте правильность введенных данных').should('be.visible')
            cy.contains('Проверьте правильность введенных данных').parent().within(() => {
                cy.get('button').click()
                cy.wait(500)
            })
            cy.contains('Проверьте правильность введенных данных').should('have.length', 0)

            cy.get('.authorization-form__block button').click()
            cy.wait(300)

            cy.contains('Проверьте правильность введенных данных').should('be.visible')
            cy.contains('Проверьте правильность введенных данных').parent().click('left')
            cy.wait(500)
            cy.contains('Проверьте правильность введенных данных').should('have.length', 0)

        })

        it('Проверка полностью корректного корректного заполнения', () => {

            // Находим поле для email
            cy.get('.authorization-form__block input[name="email"]').clear().type('marazm01@yandex.ru')
            // Находим поле для пароля
            cy.get('.authorization-form__block input[name="password"]').clear().type('ъхэжюбдлщш')
            // Проверяем ошибку над полем пароля
            cy.get('.authorization-form__block input[name="password"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })
            // Проверяем ошибку над полем email
            cy.get('.authorization-form__block input[name="email"]').parent().parent().within(() => {
                cy.get('.input__error').should('have.length', 0)
            })

            cy.get('.authorization-form__block button').should('not.have.attr', 'disabled')
            cy.get('.authorization-form__block button').click()
            cy.wait(500)

            cy.get('header .profile-block').should('be.visible')
        })

    })

    describe('Тест окошка пользователя', () => {

        it('Проверка наличия элементов и их видимости в нужный момент', () => {

            cy.get('header .profile-block').children().eq(0).should('have.attr', 'alt')
            cy.get('header .profile-block').children().eq(0).should('be.visible')

            cy.get('header .profile-block').children().eq(1).should('have.class', 'profile-block__dropdown-icon')
            cy.get('header .profile-block').children().eq(1).should('be.visible')

            cy.get('header .profile-block').children().eq(2).should('have.class', 'profile-block__profile-dropdown')
            cy.get('header .profile-block').children().eq(2).should('not.visible')

            cy.get('header .profile-block__dropdown-icon').click()

            cy.get('.profile-block__profile-dropdown .profile-block__body').should('be.visible')

            cy.get('header .profile-block__profile-dropdown').within(() => {

                cy.get('.profile-block__body').children().eq(0).should('have.class', 'profile-block__username')
                cy.get('.profile-block__body').children().eq(1).should('have.class', 'profile-block__link')
                cy.get('.profile-block__body').children().eq(2).should('have.class', 'profile-block__link--colored')
                cy.get('.profile-block__body').children().eq(3).should('have.length', 0)

                cy.get('.profile-block__link a').should('have.attr', 'href', '/profile')

            })

        })
    })

})
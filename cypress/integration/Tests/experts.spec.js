// Переменные для проверок страницы
let experts = 0 ; // Здесь хранится число экспертов для разных проверок

describe('Тест страницы Экспертный совет', () => {

    it('Заходим на страницу', () => {cy.visit('/experts')})

    it('Кликаем "Показать ещё"', () => {

        cy.get('.experts-container__expert-block').then(($expertNumber) => {
            experts = $expertNumber.length
        })

        cy.get('main button').click()
        cy.wait(500)

        cy.get('.experts-container__expert-block').then(($expertNumber) => {
            expect($expertNumber.length).to.be.greaterThan(experts)
            experts = $expertNumber.length
        })

    })

    it('Проверка уникальности эксперта', () => {
        // Берём поочерёдно всех экспертов кроме последнего
        for(let i = 0; i < experts - 1; i++) {
            // Берём заголовок карточки эксперта и помещаем его в переменную
            cy.get('.experts-container__expert-block h3').eq(i).then(($thisExpert) => {
                // Береём поочередно всех экспертов кроме тех, которых брали в родительском цикле
                for(let j = i + 1; j < experts; j++) {
                    // Берём заголовок карточки эксперта и пихаем его в переменную
                    cy.get('.experts-container__expert-block h3').eq(j).then(($anotherExpert) => {
                        // Сравниваем полученные значения. Они должны отличаться
                        expect($thisExpert.text()).to.not.equal($anotherExpert.text())

                    })

                }

            })

            
        }

    })

    it('Проверка ссылок Дмитрия Теплова', () => {

        cy.contains('Теплов Дмитрий Викторович').parent().within(() => {

            cy.get('a[href="https://krmz.info/"]').should('be.visible')
            cy.get('a[href="https://senazh.online/"]').should('be.visible')
            cy.get('a[href="https://expedition-pricep.ru/"]').should('be.visible')
            cy.get('a[href="http://frontlift.ru/"]').should('be.visible')
            cy.get('a[href="https://stl-rus.com/"]').should('be.visible')
            cy.get('a[href="https://frpperm.ru/"]').should('be.visible')
            cy.get('a[href="https://frpperm.ru/klastery/promyshlennyy-klaster-selskohozyaystvennogo-mashinostroeniya/"]').should('be.visible')
            cy.get('a[href="http://www.psu.ru/universitet/endowment-psu/struktura-fonda/popechitelskij-sovet"]').should('be.visible')
            cy.get('a[href="http://krasnokamsk.ru/Organy-vlasti/Gorodskaja-Duma"]').should('be.visible')

        })

    })

})
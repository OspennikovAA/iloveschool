// URL'ы страниц
export let progectsPage = 'https://test.iloveschool.help/projects'
export let mainPage = 'https://test.iloveschool.help/'
export let createProjectPage = 'https://test.iloveschool.help/project-info'
export let partnerPage = 'https://test.iloveschool.help/partner-info'
export let faqPage = 'https://test.iloveschool.help/faq'
export let abousUsPage = 'https://test.iloveschool.help/about'
export let expertsPage = 'https://test.iloveschool.help/experts'
export let docsPage = 'https://test.iloveschool.help/documents'
export let newsPage = 'https://test.iloveschool.help/news'
export let partnerListPage = 'https://test.iloveschool.help/partners'
export let schoolsPage = 'https://test.iloveschool.help/schools'

// URL'ы ссылок Хэдера в массиве
export let headerUrls = [
    '/', '/projects?type=all', '/project-info', '/partner-info', '/faq', '/about',
]

// URL'ы ссылок футера в двумерном  массиве
export let footerUrls = [

    ['/', 'https://deloros.ru/',],

    ['/about#team', '/experts', '/news', '/documents', '/faq',],

    ['/partners', '/schools',],

    ['mailto:info@iloveschool.help', 'tel:8 800 555 2081'],

    ['https://vk.com/iloveschoolhelp', 'https://www.facebook.com/iloveschoolhelp/', 'https://www.instagram.com/iloveschoolhelp/',],

    ['/docs/public_offer.pdf', '/docs/personal_data.pdf', '/docs/user_agreement.pdf',],

]

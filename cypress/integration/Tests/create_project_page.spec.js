import {createProjectPage} from './check_data/main_elements'
import {headerUrls} from './check_data/main_elements'
import {footerUrls} from './check_data/main_elements'

describe('Тест страницы Создать проект', () => {

    it('Заходим на страницу', () => {cy.visit("/project-info")})
    // Тест Хэдера

    describe('Тест Хэдера', () => {

        it('Проверка ЛОГО и окошка регистрации/пользователя', () => {

            cy.get('.header__logo-block').should('have.attr', 'href', headerUrls[0])
            cy.get('.header__platform-block a').should('have.length', 5)
            cy.get('div[class*="profile-block"]').should('be.visible')

        })

        for (let i = 0; i < 5; i++) {

            it('Проверка ссылок', () => {
                cy.get('.header__platform-block a').eq(i).should('have.attr', 'href', headerUrls[i+1])
            })

        }

    })

    // Тест Футера

    describe('Тест футера', () => {

        it('Тест ссылок футера', () => {

            for(let i = 0; i < footerUrls.length; i++) {
                for(let j = 0; j < footerUrls[i].length; j++) {

                    cy.get('footer').children().children().eq(i).within(() => {

                        cy.get('a').eq(j).should('have.attr', 'href', footerUrls[i][j])

                    })

                }
            }

        })

    })

    // Тест тела страницы

    describe('Тест тела страницы', () => {

        describe('Проверка наличия основных элементов', () => {            

            it('Проверка числа основных элементов', () => {

                cy.get('.project-info').children().should('have.length', 11)

            })

            it('Проверка наличия ссылок и кнопок', () => {

                cy.get('.project-info a').should('have.length', 1)
                cy.get('.project-info button').should('have.length', 1)
                cy.get('.project-info__item-link').should('have.length', 1)

            })
            
        })

        describe('Проверка работоспособности ссылок и кнопок', () => {

            it('Проверка ссылки Зарегистрируйтесь', () => {
                // Клик на "Зарегистрируйтесь"
                cy.get('.project-info__item-link').click()
                cy.get('.authorization-form__content').should('be.visible')
                // Закрываем окошко и проверяем URL
                cy.get('.authorization-form__content').within(() => {
                    cy.get('i.icon--close').click()                })
                cy.get('.authorization-form__content').should('have.length', 0)
            })

            it('Проверка ссылки Создать проект', () => {
                cy.visit("/project-info")
                // Клик на ссылку "Создать проект"
                cy.get('.project-info a').click()
                cy.get('.authorization-form__content').should('be.visible')
                // Закрываем окошко и проверяем URL
                cy.get('.authorization-form__content').within(() => {
                    cy.get('i.icon--close').click()                })
                cy.get('.authorization-form__content').should('have.length', 0)
            })

            it('Проверка кнопки Создать проект', () => {
                cy.visit("/project-info")
                // Клик на кнопку "Создать проект"
                cy.get('.project-info button').click()
                cy.get('.authorization-form__content').should('be.visible')
                // Закрываем окошко и проверяем URL
                cy.get('.authorization-form__content').within(() => {
                    cy.get('i.icon--close').click()                })
                cy.get('.authorization-form__content').should('have.length', 0)

            })

        })

    })

})
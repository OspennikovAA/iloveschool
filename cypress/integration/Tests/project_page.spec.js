let firstItem;
let secondItem; 
let lastItem;
let preLastItem;

describe('Тест страницы Проекты', () => {

    it('Заходим на страницу', () => {cy.visit('/projects')})

    it('Наличие поля ввода', () => {

        cy.get('input.search-input__field').should('be.visible')

    })

    it('Наличие иконки поиска', () => {

        cy.get('button.search-input__icon').should('be.visible')

    })

    it('Наличие сортировки от ранних к поздним и наоборот', () => {

        cy.get('.search-project-form__right-section').should('be.visible')

    })

    it('Наличие карточек', () => {

        cy.get('.project-card').eq(0).should('be.visible')

    })

    it('Наличие кнопки Показать ещё', () => {

        cy.get('button.btn__show-more').should('be.visible')

    })

})

describe('Проверка сортировки', {retries:3}, () => {

    it('Смотрим сколько карточек', () => {
        cy.get('.search-project-form__projects-container .project-card').should('have.length', 12)
    })

    it('Раскрываем список', () => {
        cy.get('button.btn__show-more').click()
    })

    it('Смотрим сколько карточек теперь', () => {
        cy.wait(500)
        cy.get('.search-project-form__projects-container .project-card').then((cards)=>{
            expect(cards.length).to.be.greaterThan(12)
        })
    })

    it('Тестируем сортировку', () => {

        // Пихаем текст карточек в соответствующие переменные

        cy.get('.search-project-form__projects-container .project-card').then((cards)=>{

            cy.get('.search-project-form__projects-container').within(() => {

                cy.get('.project-card').eq(0).children().eq(0).children().children().eq(2).then(($lang) => {

                    firstItem = $lang.text()

                })

                cy.get('.project-card').eq(1).children().eq(0).children().children().eq(2).then(($lang) => {

                    secondItem = $lang.text()

                })

                cy.get('.project-card').eq(cards.length-2).children().eq(0).children().children().eq(2).then(($lang) => {

                    preLastItem = $lang.text()

                })

                cy.get('.project-card').eq(cards.length-1).children().eq(0).children().children().eq(2).then(($lang) => {

                    lastItem = $lang.text()

                })
                
            })

            // Меняем сортировку

            cy.get('.search-project-form__right-section').click()
            cy.wait(2000)

            // Проверяем текст карточек

            cy.get('.search-project-form__projects-container').within(() => {

                cy.get('.project-card').eq(0).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(lastItem)

                })

                cy.get('.project-card').eq(1).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(preLastItem)

                })

                cy.get('.project-card').eq(cards.length-2).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(secondItem)

                })

                cy.get('.project-card').eq(cards.length-1).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(firstItem)

                })
                
            })

            // Меняем сортировку

            cy.get('.search-project-form__right-section').click()
            cy.wait(2000)

            // И снова проверяем текст карточек

            cy.get('.search-project-form__projects-container').within(() => {

                cy.get('.project-card').eq(0).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(firstItem)

                })

                cy.get('.project-card').eq(1).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(secondItem)

                })

                cy.get('.project-card').eq(cards.length-2).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(preLastItem)

                })

                cy.get('.project-card').eq(cards.length-1).children().eq(0).children().children().eq(2).then(($lang) => {

                    expect($lang.text()).to.eq(lastItem)

                })
                
            })
        })

    })

})
describe('Тест страницы FAQ', () => {

    it('Заходим на страницу', () => {cy.visit('/faq')})

    it('Проверка числа вопросов', () => {

        cy.get('main section').should('have.length', 8)
        
        for (let i = 0; i < 8; i++) {
        cy.get('section').eq(i).within(() => {
            cy.contains('?').should('be.visible')
        })
        }
    })

    it('Проверка ссылок', () => {

        cy.contains('Как создать проект?').parent().within(() => {
            cy.get('a').eq(0).should('have.attr', 'href', 'https://iloveschool.help/projects/new')
            cy.get('a').eq(1).should('have.attr', 'href', 'mailto:info@iloveschool.help')
        })

    })

})
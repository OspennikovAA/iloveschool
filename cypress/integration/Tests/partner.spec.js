describe('Тест страницы Стать партнёром', () => {

    it('Заходим на страницу', () => {cy.visit('/partner-info')})

    it('Основной блока тела', () => {

        cy.get('main').children().should('have.length', 11)

    })

    it('Блок проверки проектов', () => {

        cy.get('main .become-partner__stages').within(() => {
            cy.get('.become-partner__stage').should('have.length', 4)
        })

    })

    it('Ссылки и кнопки', () => {

        cy.get('main').within(() => {
            cy.get('a[href="/schools"]').should('be.visible')
            cy.get('a[href="/projects"]').should('be.visible')
            cy.get('a[href="/docs/Реквизиты_компании.pdf"]').should('be.visible')
            cy.get('button.create-project__right-button').should('be.visible')
        })

    })

    it('Правильность ссылки кнопки', () => {

        cy.get('main button.create-project__right-button').click()
        cy.url().should('contains', 'https://test.iloveschool.help/projects')

    })
})
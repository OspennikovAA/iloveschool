import {headerUrls} from './check_data/main_elements'
import {footerUrls} from './check_data/main_elements'

describe('Тест Главной страницы', () => {

    it('Заходим на страницу', () => {cy.visit('/')})

    // Тест Хэдера

    describe('Тест Хэдера', () => {

        it('Проверка ЛОГО и окошка регистрации/пользователя', () => {

            cy.get('.header__logo-block').should('have.attr', 'href', headerUrls[0])
            cy.get('.header__platform-block a').should('have.length', 5)
            cy.get('div[class*="profile-block"]').should('be.visible')

        })

        for (let i = 0; i < 5; i++) {

            it('Проверка ссылок', () => {
                cy.get('.header__platform-block a').eq(i).should('have.attr', 'href', headerUrls[i+1])
            })

        }

    })

    // Тест Футера

    describe('Тест футера', () => {

        it('Тест ссылок футера', () => {

            for(let i = 0; i < footerUrls.length; i++) {
                for(let j = 0; j < footerUrls[i].length; j++) {

                    cy.get('footer').children().children().eq(i).within(() => {

                        cy.get('a').eq(j).should('have.attr', 'href', footerUrls[i][j])

                    })

                }
            }

        })

    })

    // Тест тела страницы

    describe('Тест тела страницы', () => {
        
        describe('Тест блока активные проекты', () => {

            it('Проверяем что есть хотя бы 4 карточки с не пустой ссылкой на какой-либо проект', () => {

                cy.get('.active-projects__projects-container').within(() => {
                    for( let i = 0; i < 4; i++) {
                        cy.get('a[href*="/projects/"]').eq(i).should('be.visible')
                    }
                })
                
            })

            it('Проверяем наличие кнопочки со стрелочкой и что ссылка ведёт куда надо', () => {
                cy.get('.active-projects__head-container').within(() => {
                    cy.contains('Все проекты').should('have.attr', 'href', '/projects?type=all').within(()=>{
                        cy.get('img[src*=".svg"]').should('be.visible')
                    })
                })
            })

        })
        
        describe('Тест Подписки', () => {

            describe('Тесты НЕкорректного заполнения формы', () => {

                it('Тест email без @ чекбокс ВЫКЛ', () => {

                    cy.get('input[name="email"]').clear().type('testsibdev.ru')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест email без домена чекбокс ВЫКЛ', () => {

                    cy.get('input[name="email"]').clear().type('test@sibdev')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест email без @ и домена чекбокс ВЫКЛ', () => {

                    cy.get('input[name="email"]').clear().type('testsibdev')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест email без текста перед @ чекбокс ВЫКЛ', () => {

                    cy.get('input[name="email"]').clear().type('@testsibdev.ru')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест корректного email чекбокс ВЫКЛ', () => {

                    cy.get('input[name="email"]').clear().type('test@sibdev.ru')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Простановка чекбокса', () => {

                    cy.get('.subscribe-form__check-input').click({force: true})

                })

                it('Тест email без @ чекбокс ВКЛ', () => {

                    cy.get('input[name="email"]').clear().type('testsibdev.ru')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест email без домена чекбокс ВКЛ', () => {

                    cy.get('input[name="email"]').clear().type('test@sibdev')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест email без @ и домена чекбокс ВКЛ', () => {

                    cy.get('input[name="email"]').clear().type('testsibdev')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

                it('Тест email без текста перед @ чекбокс ВКЛ', () => {

                    cy.get('input[name="email"]').clear().type('@testsibdev.ru')
                    cy.get('.subscribe-form__button button').should('have.attr', 'disabled')

                })

            })

            describe('Тесты корректного заполнения формы', () => {

                it('Тест корректного email чекбокс ВКЛ', () => {

                    cy.get('input[name="email"]').clear().type('test@sibdev.ru')
                    cy.get('.subscribe-form__button button').should('not.have.attr', 'disabled')

                })

            })

        })
        
        describe('Тест блока создания проекта', () => {

            it('Тест кнопки создания проекта', () => {

                cy.get('.create-project__container').within(() => {
                    cy.contains('Создать проект').click()
                })

                cy.get('.authorization-form__content').should('be.visible')
            
            })

            it('Тест закрытия окна авторизации', () => {
                
                cy.get('.authorization-form__content').within(() => {
                    cy.get('i.icon--close').click()
                })

                cy.get('.authorization-form__content').should('have.length', 0)

            })

        })
        
        describe('Тест поиска', () => {
            
            it('Тест работоспособниости поиска', () => {

                cy.get('input[placeholder="Напишите город, инициатора или название проекта."]').type('школа')
                cy.get('form button.search-input__icon').click()
                cy.wait(1000)

                cy.url().should('contains', 'https://test.iloveschool.help/projects')

            })

        })

    })

})
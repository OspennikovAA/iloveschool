describe('Тест страницы Школы', () => {

    it('Заходим на страницу', () => {cy.visit('/schools')})

    it('Проверка структуры страницы', () => {
        // Проверяем заголовок
        cy.get('main').children().eq(0).children().eq(0).children().eq(0).should('be.visible')
        cy.get('main').children().eq(0).children().eq(0).children().eq(0).should('contain', 'Инициаторы проектов')
        // Проверяем поле ввода и кнопку поиска
        cy.get('main').children().eq(0).children().eq(0).children().eq(1).within(() => {

            cy.get('input').should('be.visible')
            cy.get('button').should('be.visible')

        })
        // Проверяем наличие контейнера для школ
        cy.get('main').children().eq(0).children().eq(0).children().eq(2).should('be.visible')
        cy.get('main').children().eq(0).children().eq(0).children().eq(2).should('have.class', 'school-card__schools-container')
        // Проверяем наличие кнопки "Показать ещё"
        cy.get('main').children().eq(0).children().eq(0).children().eq(3).within(() => {
            cy.get('button').should('be.visible')
        })

    })

    it('Тест кнопки "Показать ещё"', () => {
        // Ищем все карточки школ и запихиваем в переменную
        cy.get('main .school-card').then(($before) => {
            // Тык на кнопку
            cy.get('main .btn__show-more').click()
            cy.wait(3000)
            // Ещё раз ищем все карточки школ и в другую переменную их
            cy.get('main .school-card').then(($after) => {
                // Проверяем что стало больше, чем было
                expect($after.length).to.be.greaterThan($before.length)
            })

        })

    })

    it('Проверка структуры карточек', () => {
        // Берём все карточки и в переменную их. Она понадобится для цикла
        cy.get('main .school-card').then(($num) => {
            // Перебираем все карточки
            for(let i = 0; i < $num.length; i++) {

                cy.get('.school-card').eq(i).within(() => {
                    // Проверка наличия не пустой картинки в аватарке 
                    cy.get('.school-card__avatar img').should('have.attr', 'src')
                    cy.get('.school-card__avatar img').should('not.have.attr', 'src', '')
                    // Проверка наличия заголовка школы и ссылки на её страницу на сайте
                    cy.get('.school-card__name').within(() => {
                        cy.get('a[href*="/school/"]').should('be.visible')
                    })
                    cy.get('.school-card__name h1').should('be.visible')
                    // Проверка наличия не пустого поля Город
                    cy.get('.school-card__city').then(($city) => {
                        expect($city.text()).not.to.equal('')
                    })
                    // Проверка ссылки К проектам школы
                    cy.get('.school-card__url').within(() => {
                        cy.get('a[href*="/school-projects/"]').should('be.visible')
                        cy.get('span').then(($text) => {
                            expect($text.text()).to.be.equal('К проектам инициатора')
                        })
                    })

                })

            }

        })

    })

    it('Проверка страницы проектов инициатора', ()=>{
        cy.contains('К проектам инициатора').click()
        cy.url().should('includes', '/school-projects/')
        cy.contains('Проекты инициатора').should('be.visible')
    })

})
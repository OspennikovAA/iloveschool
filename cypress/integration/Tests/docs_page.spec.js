describe('Тест страницы Документы', () => {

    it('Заходим на страницу', () => {cy.visit('/documents')})

    it('Проверка наличия ИНН и ОГРН', () => {

        cy.contains('АНО Платформа ОП (ИНН+ОГРН)').parent().within(() => {
            cy.get('img[src="/images/docs/ils_свидетельство-1.png"]').should('be.visible')
            cy.get('a[href="/docs/АНО_Платформа_ОП_(ИНН+ОГРН).PDF"]').should('be.visible')
        })

    })

    it('Проверка наличия свидетельсва МИНЮСТа', () => {

        cy.contains('АНО Платформа ОП (МИНЮСТ)').parent().within(() => {
            cy.get('img[src="/images/docs/ils_свидетельство-2.png"]').should('be.visible')
            cy.get('a[href="/docs/АНО_Платформа_ОП_(МИНЮСТ).PDF"]').should('be.visible')
        })

    })

    it('Проверка наличия Устава', () => {

        cy.contains('АНО Платформа ОП (УСТАВ)').parent().within(() => {
            cy.get('img[src="/images/docs/ils_устав.png"]').should('be.visible')
            cy.get('a[href="/docs/АНО_Платформа_ОП_(УСТАВ).PDF"]').should('be.visible')
        })

    })

    it('Проверка наличия выписки из ЕГЮРЛ', () => {

        cy.contains('Выписка из ЕГРЮЛ').parent().within(() => {
            cy.get('img[src="/images/docs/Выписка_из_ЕГРЮЛ.png"]').should('be.visible')
            cy.get('a[href="/docs/Выписка_из_ЕГРЮЛ.PDF"]').should('be.visible')
        })

    })

    it('Проверка наличия Годового отчёта за 2019', () => {

        cy.contains('Годовой отчет за 2019').parent().within(() => {
            cy.get('img[src="/images/docs/Годовой_отчет.png"]').should('be.visible')
            cy.get('a[href="/docs/Годовой_отчет_2019.pdf"]').should('be.visible')
        })

    })

    it('Проверка наличия Реквизитов', () => {

        cy.contains('АНО Платформа ОП (РЕКВИЗИТЫ)').parent().within(() => {
            cy.get('img[src="/images/docs/Реквизиты.png"]').should('be.visible')
            cy.get('a[href="/docs/Реквизиты_компании.pdf"]').should('be.visible')
        })

    })

})